/*
Sylwia Gajtkowska
Zadanie 4: Lenna
Opis: Lenna to zdj�cie u�ywane jako obraz testowy w kompresji obrazow. Zadanie polega�o na zliczeniu kolorow z fotografii.
W tym celu postanowi�am u�y� stosu vector, poniewa� takie (nowe dla mnie) rozwi�zanie wydawa�o si� by� najlepsze. 
*/

#include "stdafx.h"
#include "ppm.h"
#include <iostream>
#include <vector>
#include <algorithm>

// Deklaracja metody liczKolory
int liczKolory(Obraz&);

// Implementacja metody liczKolory, ktora zlicza tylko unikalne kolory
// Metoda zwraca wielkosc wektora w formie integera
int liczKolory(Obraz& obraz)
{
	// Tworze tymczasowy wektor obiektow klasy Color o nazwie counter
	// Przypisuje do niego wektor z obiektu obraz
	// http://cpp0x.pl/dokumentacja/standard-C++/vector/819
	std::vector<Color> counter = obraz.getWektorKolorow();

	// Korzystajac z unique z <algorithm>, usuwam powtarzajace sie elementy korzystajac z metody erase
	// begin() - wskazuje pierwszy element dynamicznej tablicy
	// end() - wskazuje na koniec dynamicznej tablicy
	// size() - zwraca ilo�� element�w tablicy
	// erase() - Usuwa jeden element lub wiele element�w z kontenera vector wyst�puj�cych na podanej pozycji lub w podanym zakresie. 

	counter.erase(std::unique(counter.begin(), counter.end()), counter.end());


	// Wyswietla wszystkie elementy, razem z potworzeniami
	// std::cout << "Z powtorzeniami = " << obraz.getWektorKolorow().size() << std::endl;

	return counter.size();
}

int main()
{
	Obraz * ob;
	int kolor;

	// Magia polimofrizmu
	// A wi�c do wskaznika typu Obraz, przypisujemy obiekt klasy PPM, ktora dziedziczy po klasie Obraz?
	// Podczas tworzenia obiektu PPM, wywolywana jest metoda loadImage, ktora troche trwa, dlatego wypisanie na ekran pojawi sie po jakims czasie
	ob = new PPM("Lena.ppm");
	kolor = liczKolory(*ob);

	std::cout << "Unikalnych kolorow = " << kolor;

	delete ob;

	system("pause");

	return 0;
}