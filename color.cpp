#include "stdafx.h"
#include "color.h"

// Konstruktor bezparametrowy
Color::Color()
{
	this->redColor = 0;
	this->greenColor = 0;
	this->blueColor = 0;
}

// Konstruktor przyjmujacy wartosci kolorow jako parametry
Color::Color(unsigned int redColor, unsigned int greenColor, unsigned int blueColor)
{
	this->redColor = redColor;
	this->greenColor = greenColor;
	this->blueColor = blueColor;
}

// Konstruktor kopiujacy
Color::Color(const Color& color)
{
	this->redColor = color.redColor;
	this->greenColor = color.greenColor;
	this->blueColor = color.blueColor;
}

// Destruktor
Color::~Color() {}

// Gettery
unsigned int Color::getRedColor()
{
	return this->redColor;
}

unsigned int Color::getGreenColor()
{
	return this->greenColor;
}

unsigned int Color::getBlueColor()
{
	return this->blueColor;
}

// Settery
void Color::setRedColor(unsigned int redColor)
{
	this->redColor = redColor;
}

void Color::setGreenColor(unsigned int greenColor)
{
	this->greenColor = greenColor;
}

void Color::setBlueColor(unsigned int blueColor)
{
	this->blueColor = blueColor;
}

// Operator przypisania

Color& Color::operator =(const Color& color)
{
	this->setRedColor(color.redColor);
	this->setGreenColor(color.greenColor);
	this->setBlueColor(color.blueColor);

	return *this;
}

// Operator porownania
bool Color::operator == (const Color& color)
{
	if (this->redColor != color.redColor || this->greenColor != color.greenColor || this->blueColor != color.blueColor)
		return false;

	return true;
}

// Operator nierownosci
bool Color::operator != (const Color& color)
{
	return !operator ==(color);
}

// Operator wypisania na ekran
std::ostream& operator<< (std::ostream& os, const Color& color)
{
	os << color.redColor << " " << color.greenColor << " " << color.blueColor;
	return os;
}

// Operator wejscia, wpisuje dane do obiektu klasy Color
// Potrzebny w klasie PPM do wpisania wartosci pobranych z pliku
std::istream& operator>> (std::istream& is, Color& color)
{
	int r, g, b;
	is >> r >> g >> b;

	color.setRedColor(r);
	color.setGreenColor(g);
	color.setBlueColor(b);

	return is;
}